+++
categories = ["Announcement", "Sponsorship"]
date = "2016-11-08T11:25:07-02:00"
description = "Encerradas as cotas de patrocínio!"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Patrocínio"

+++

Felizmente encerramos as quotas de patrocínio.

Tivemos muito bons retornos para parceria na realização do evento de empresas que são referência no mundo da tecnologia e, principalmente, no mundo **DevOps**.

Agradecemos pelo patrocínio:

- <i class="gold"> Gold </i>:  ***Rancher*** e ***Puppet***
- <i class="silver"> Silver </i>:  ***Gitlab*** e ***Redhat***

No nosso entendimento mostra o alinhamento e aderência destes grandes *players* com o tema **DevOps** e em apoiar seus parceiros na eficiência da entrega de serviços a seus clientes ou cidadãos.

O nosso **MUITO OBRIGADO**.

Nos encontramos no evento!

PS: *já contamos com vocês também para o ano que vem! ;)*
