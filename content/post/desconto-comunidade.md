+++
date = "2016-10-05T12:57:42-03:00"
categories = ["Announcement", "News"]
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Proposals"]
draft = false
image = "img/post-bg.jpg"
title = "Cupons de desconto para comunidades"
description = "Puppet-BR, Docker-BR e DevOps-BR até 50% de desconto"

+++

Se você quer fazer sua inscrição com desconto, procure as comunidades no telegram.

- http://telegram.me/puppetbr
- http://telegram.me/dockerbr
- http://telegram.me/devopsbr

Os admins destas comunidades tem cupons com até 50% de desconto, aproveite!
