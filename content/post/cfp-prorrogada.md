+++
date = "2016-10-05T08:57:42-03:00"
categories = ["Announcement", "News"]
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Proposals"]
draft = false
image = "img/post-bg.jpg"
title = "Chamada de trabalhos prorrogada"
description = "Chamada de trabalhos prorrogada para 09/Oct, aproveite!"

+++

A chamada de trabalhos foi prorrogada para dia 09 de Outubro, aproveite e submeta sua proposta.