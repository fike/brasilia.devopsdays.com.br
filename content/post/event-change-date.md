+++
categories = ["Announcement", "News"]
date = "2016-08-26T16:07:26-03:00"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Data do evento mudou"
description = "Agora será no dia 18 de Novembro"

+++

Houve necessidade e de mudarmos a data do evento!

Agora a data do evento é:
<span class="alert alert-danger">18 de novembro de 2016</span>

Acreditamos que esta será uma melhor data para todos.
