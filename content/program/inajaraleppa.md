+++
categories = ["Talk", "Palestra"]
date = "2016-09-05T18:59:19-03:00"
description = "Inajara Leppa"
draft = false
tags = ["DevOps", "Automation", "Metrics"]
title = "inajaraleppa"

copalestrante_name = "Bárbara Hartmann"
copalestrante_bio = ""
copalestrante_website = ""
copalestrante_twitter = ""
copalestrante_photoUrl = "https://scontent-gru.xx.fbcdn.net/v/t1.0-9/fr/cp0/e15/q65/13495325_1030303857048138_5912214820898887758_n.jpg?efg=eyJpIjoidCJ9&oh=4410594023b027e56c285a546bd89e85&oe=587F8262"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
#  <span class="text text-success">Produção nove vezes ao dia - Como um time da *rackspace* torna isso possível?</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Vamos apresentar quais técnicas e ferramentas são usadas no nosso time para garantir que as entregas em produção de um projeto com 5 anos de vida tenham valor e qualidade e sejam feitas 9 vezes ao dia, 5 dias por semana, 12 meses ao ano. Espere ouvir sobre pipeline, testes, automação, revisão de código, branch based development, multiple single page applications, Pikachu, ChatOps, Hackdays e feature toggles.

<!--
Temas das trilhas:
  <span class="text-primary"><b>[C]</b>ulture (Cultura)</span>
  <span class="text-success"><b>[A]</b>utomation (Automação)</span>
  <span class="text-info"><b>[M]</b>etrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning"><b>[S]</b>haring (compartilhamento)</span>
-->

- **Trilha**: <span class="text text-success">Automation (automação)</span>
- **Público alvo**: Iniciante
